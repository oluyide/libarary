defmodule Vueshatter.DataAccess do
  @moduledoc """
  The Accounts context.
  """
  import Ecto.Query, warn: false
  alias Vueshatter.Repo

  # alias Vueshatter.Models.LoanHistory
  alias Vueshatter.Models.Book
  alias Vueshatter.Models.User
  alias Vueshatter.Models.LoanHistory


  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def create_books(attrs \\ %{}) do
    %Book{}
    |> Book.changeset(attrs)
    |> Repo.insert()
  end

  def create_loans(attrs \\ %{}) do
    %LoanHistory{}
    |> LoanHistory.changeset(attrs)
    |> Repo.insert()
  end

  def select_mybooks(model, my_id) do
    from l in model,
    join: b in Book,
    on: b.id == l.book_id,
    where: l.user_id == ^my_id,
    select: l
  end
  def get_my_books!(my_id) do
    #from(driver_loc in query, where: driver_loc.status == "Invisible", order_by: fragment("? <-> ST_SetSRID(ST_MakePoint(?,?), ?)", driver_loc.driver_lat_long, ^lng, ^lat, ^srid))
    from orders  in LoanHistory , where: orders.user_id == ^my_id
   end

  def my_books!(my_id), do: Repo.get_by(LoanHistory, user_id: my_id)

  def get_loan!(id), do: Repo.get!(LoanHistory, id)
  def get_Book!(id), do: Repo.get!(Book, id)

  def update_loan(%LoanHistory{} = loan, attrs) do
    loan
    |> LoanHistory.changeset(attrs)
    |> Repo.update()
  end


end
