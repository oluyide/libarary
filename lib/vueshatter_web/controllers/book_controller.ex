defmodule VueshatterWeb.BookController do
  use VueshatterWeb, :controller
  alias Vueshatter.{Repo, Models.Book, DataAccess, Models.LoanHistory,Model.User}
  # alias Ecto.{Changeset}

  # alias Vueshatter.Models.AuctionItem

  def index(conn, _params) do
    books =  DataAccess.get_my_books!(1)|> Repo.all |> Repo.preload(:book)
   #  IO.puts("==========")
   #  IO.inspect(books)
   #  IO.puts("==========")
    render(conn, "index.html", books: books)
   end

   def show(conn, %{"id" => id}) do
   result = DataAccess.get_loan!(id)
   book = DataAccess.get_Book!(result.id)

   extentionA = %{:return_date => Date.add(result.return_date, 7), :extendedtime => result.extendedtime+1}
   extentionB = %{:return_date => Date.add(result.return_date,14), :extendedtime => result.extendedtime+1}
   extentionC = %{:return_date => Date.add(result.return_date, 21), :extendedtime => result.extendedtime+1}
   if(result.extendedtime < 4 && book.code == "A") do
      with {:ok, %LoanHistory{} = _loan} <- DataAccess.update_loan(result, extentionA) do
         conn |> put_flash(:info, "Extension accepted")
              |> redirect(to: "/")
   end
   else if(result.extendedtime < 4 && book.code == "B") do
   with {:ok, %LoanHistory{} = _loan} <- DataAccess.update_loan(result, extentionB) do
      conn |> put_flash(:info, "Extension accepted")
           |> redirect(to: "/")
end
else if(result.extendedtime < 4 && book.code == "C") do
   with {:ok, %LoanHistory{} = _loan} <- DataAccess.update_loan(result, extentionC) do
      conn |> put_flash(:info, "Extension accepted")
           |> redirect(to: "/")
end
else
   conn
   |> put_flash(:error, "Extension rejected")
   |> redirect(to: "/")
end
#    case result.extendedtime < 4  do
#     true ->
#    with {:ok, %LoanHistory{} = _loan} <- DataAccess.update_loan(result, extentionA) do
#    conn |> put_flash(:info, "Extension accepted")
#         |> redirect(to: "/")
#    end
#   false ->
#      conn
#       |> put_flash(:error, "Extension rejected")
#       |> redirect(to: "/")
#    end
end
end
end
end
