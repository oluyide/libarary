defmodule Vueshatter.Models.Book do
  use Ecto.Schema
  import Ecto.Changeset


  schema "books" do
      field :code, :string
      field :title, :string
      has_many :loan_histories,  Vueshatter.Models.LoanHistory
    timestamps()
  end

  @doc false
  def changeset(book, attrs) do
    book
    |> cast(attrs, [:code, :title])
    |> validate_required([:code, :title])
  end
end
