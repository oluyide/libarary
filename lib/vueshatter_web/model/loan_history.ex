defmodule Vueshatter.Models.LoanHistory do
  use Ecto.Schema
  import Ecto.Changeset
  alias Vueshatter.Models.Book
  alias Vueshatter.Models.User


  schema "loan_histories" do
    field :start_date, :date
    field :return_date, :date
    field :extendedtime, :integer
    belongs_to :user, User
    belongs_to :book, Book
    timestamps()
  end

  @doc false
  def changeset(loan_history, attrs) do
    loan_history
    |> cast(attrs, [:start_date, :return_date, :extendedtime, :user_id, :book_id])
    |> validate_required([:start_date, :return_date, :extendedtime, :user_id, :book_id])
  end
end
