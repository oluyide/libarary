defmodule Vueshatter.Models.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :username, :string
    has_many :loan_histories,  Vueshatter.Models.LoanHistory
    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username])
    |> validate_required([:username])
  end
end
