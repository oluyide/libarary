Feature: Libarary Loan
  As a student
  Such that I have burrowed book
  And I want to Extend it

  Scenario: Burrowing with extension success
    Given the following books are available
          | title                          | start_date | return_date   |
          | Programming Phoenix 1.3        | 2019-01-09 | 2019-01-16    |
          | Secret of the Javascript ninja | 2019-01-09 | 2019-01-16    |
    And I want to extend the "Programming Phoenix 1.3" by a week
    And I open extension web page
    When extend button is clicked
    Then I should a success receive flash message


Scenario: Burrowing with extension failure due to 4 times
    Given the following books are available
          | title                           | Code| start_date | return_date   | Time Extended|
          | Programming Phoenix 1.3         | A   | 2019-01-09 | 2019-01-16    |0             |
          | Secret of the Javascript ninja  | A   | 2019-01-09 | 2019-01-16    |4             |
    And I want to extend the "Secret of the Javascript ninja" by a week
    And I open extension web page
    When extend button is clicked
    Then I should a receive failure flash message