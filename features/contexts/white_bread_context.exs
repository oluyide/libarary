defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    %{}
  end
  scenario_finalize fn _status, _state ->
    nil
    #Hound.end_session
  end

  given_ ~r/^the following books are available$/, fn state ->
    {:ok, state}
  end


  and_ ~r/^I want to extend the "(?<book_name>[^"]+)" by a week$/,
  fn state, %{book_name: book_name} ->
    state = Map.put(state, :book_name, book_name)
    {:ok, state}
  end

  and_ ~r/^I open extension web page$/, fn state ->
    navigate_to "/"
    Process.sleep 2000
    {:ok, state}
  end



  when_ ~r/^extend button is clicked$/, fn state ->
    row = find_all_elements(:tag, "tr")|> Enum.find(&(visible_text(&1)=~state[:book_name]))
    find_within_element(row, :class, "btn-xs") |> click
    {:ok, state}
  end

  then_ ~r/^I should receive flash message$/, fn state ->
    assert visible_in_page? ~r/Extension accepted/
    {:ok, state}
  end
end
