defmodule Vueshatter.Repo.Migrations.Loanhistory do
  use Ecto.Migration

  def change do
    create table(:loan_histories) do
    add :start_date, :date
    add :return_date, :date
    add :extendedtime, :integer, default: 0, null: false
    add :user_id, references(:users)
    add :book_id, references(:books)
    timestamps()
  end
  create index(:loan_histories, [:user_id])
  create index(:loan_histories, [:book_id])
end
end
