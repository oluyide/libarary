# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Vueshatter.Repo.insert!(%Vueshatter.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Vueshatter.DataAccess


user = [
    %{
      username: "My Name",
    }

  ]
Enum.each(user, fn(data) ->
  DataAccess.create_user(data)
  end)


  books = [
    %{
      code: "A",
      title: "Programming Phoenix 1.3     ",

    },
    %{
      code: "A",
      title: "Secret of the Javascript ninja",

    },
    %{
      code: "B",
      title: "ok tester",

    },
    %{
      code: "B",
      title: "tesing for B",

    },

    %{
      code: "C",
      title: "Testing for C",

    },
    %{
      code: "C",
      title: "Testing for C 2",

    }


  ]
Enum.each(books, fn(data) ->
  DataAccess.create_books(data)
  end)


loans = [
    %{
    start_date: Date.utc_today,
    return_date: Date.add(Date.utc_today, 7),
    extendedtime: 0,
    user_id: 1,
    book_id: 1
    },
    %{
      start_date: Date.utc_today,
      return_date: Date.add(Date.utc_today, 7),
      extendedtime: 0,
      user_id: 1,
      book_id: 2
    },

    %{
      start_date: Date.utc_today,
      return_date: Date.add(Date.utc_today, 7),
      extendedtime: 0,
      user_id: 1,
      book_id: 3
      },
      %{
        start_date: Date.utc_today,
        return_date: Date.add(Date.utc_today, 7),
        extendedtime: 0,
        user_id: 1,
        book_id: 4
      },
      %{
        start_date: Date.utc_today,
        return_date: Date.add(Date.utc_today, 7),
        extendedtime: 0,
        user_id: 1,
        book_id: 5
        },
        %{
          start_date: Date.utc_today,
          return_date: Date.add(Date.utc_today, 7),
          extendedtime: 0,
          user_id: 1,
          book_id: 6
        }



  ]

  Enum.each(loans, fn(data) ->
    DataAccess.create_loans(data)
      end)





