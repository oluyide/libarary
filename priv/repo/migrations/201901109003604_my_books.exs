defmodule Vueshatter.Repo.Migrations.MyBooks do
  use Ecto.Migration

  def change do

    create table(:books) do
      add :code, :string
      add :title, :string
      timestamps()
  end
end
end
